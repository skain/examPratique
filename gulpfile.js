var gulp = require('gulp');
var autoprefix = require('gulp-autoprefixer');
var csso = require('gulp-csso');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var del = require('del');


gulp.task('clean', function() {
  del(['.tmp', 'dist/*', '!dist/.git'], { dot: true });
});

gulp.task('copy', function() {
  gulp.src([
    'src/*',
		'!src/scss/',
  ], {
    dot: true,
  })
  .pipe(gulp.dest('dist'));
});


gulp.task('style', function(){
  gulp.src('src/scss/**/*.scss')
  .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(autoprefix({
      browsers: ['last 5 versions']
    }))
    .pipe(csso())//6
  .pipe(sourcemaps.write('.'))
  .pipe(gulp.dest('dist'))
  .pipe(gulp.dest('.tmp'));
});

gulp.task('html', function () {
  gulp.src('src/**/*.html')
    .pipe(gulp.dest('dist'));
});


gulp.task('default', ['clean', 'copy', 'style' ]);
